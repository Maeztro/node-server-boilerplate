const path = require('path');
const PORT = process.env.PORT || 3000;

//express
var express = require('express');

//cors
var cors=require('cors');

//passport
var passport = require('passport');
require('./app_components/config/passport');
require('dotenv').config();

//parsers
var body_parser = require('body-parser');
var cookie_parser = require('cookie-parser');

//mongo
var mongoose = require('mongoose');
//if you use mongo atlas, put your db cloud uri here
// var cloud_uri = ''
// mongoose.connect(cloud_uri);
//otherwise if you run your db locally use this one instead
mongoose.connect('mongodb://localhost/item');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
});

//set app
var app = express();

//set routes
var routes = require('./app_components/routes/index');

//middleware
app.use(cors());
app.use(body_parser.json());
app.use(body_parser.urlencoded({extended:false}));
app.use(cookie_parser());

//public www folder
app.use(express.static("../www"));
app.use('/www', express.static(__dirname + '/www'));

// passport init
app.use(passport.initialize());
app.use(passport.session());

//routes
app.use('/', routes);

//error handling
app.use(function(err,req,res,next){
    if(err.name==='UnauthorizedError'){
        res.status('401');
        res.json({"error":err.name+" : "+err.message});
    }
});

//setting a default view
app.use(function(err,req,res,next){
    res.status(404).send({error:err.message});
});

//ports
app.listen(PORT,function(){console.log('Listening on '+ PORT);});
