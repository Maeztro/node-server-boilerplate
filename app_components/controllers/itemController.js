var sendJSONresponse=function(res,status,content){
    res.status(status);
    res.json(content);
};

var Item = require('../models/items');

function getItems(req,res,next){
    //console.log('in controller: getItems recieved',req.body);
    Item.getItems()
        .then(function(resp){
            sendJSONresponse(res,200,{
                "items":resp
            });
        },function(err){
            sendJSONresponse(res,404,err);
        }).catch(next);
}

function getItemById(req,res,next){
    //console.log('in controller: getItemById recieved',req.body);
    var id = req.params._id;
    Item.getItemById(id)
        .then(function(resp){
            sendJSONresponse(res,200,{
                "item":resp
            });
        },function(err){
            sendJSONresponse(res,404,err);
        }).catch(next);
}

function addItem(req,res,next){
    //console.log('in controller: addItem recieved',req.body);
    var item = req.body;
    Item.addItem(item)
        .then(function(resp){
            //console.log('added item',resp,'to the db');
            sendJSONresponse(res,200,{
                "message":'added new entry'
            });
        },function(err){
            sendJSONresponse(res,404,err);
        }).catch(next);
}

function updateItem(req,res,next){
    //console.log('in controller: updateItem recieved',req.body);
    //var id = req.params._id;
    var item = req.body;
    Item.updateItem(item)
        .then(function(resp){
            //console.log('added item',resp,'to the db');
            sendJSONresponse(res,200,{
                "message":'updated entry'
            });
        },function(err){
            sendJSONresponse(res,404,err);
        }).catch(next);
}

function deleteItem(req,res,next){
    //console.log('in controller: deleteItem recieved',req.body);
    var id = req.params._id;
    Item.deleteItem(id)
        .then(function(resp){
            //console.log('added item',resp,'to the db');
            sendJSONresponse(res,200,{
                "message":'deleted entry'
            });
        },function(err){
            sendJSONresponse(res,404,err);
        }).catch(next);
}

module.exports={
    getItems:getItems,
    getItemById:getItemById,
    addItem:addItem,
    updateItem:updateItem,
    deleteItem:deleteItem
};
