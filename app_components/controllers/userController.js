var passport=require('passport'),
    crypto=require('crypto'),
    jwt=require('jsonwebtoken');

var sendJSONresponse=function(res,status,content){
    res.status(status);
    res.json(content);
};

var User = require('../models/users');

function generateJwt(user){
    var expiry=new Date();
    expiry.setDate(expiry.getDate()+1); //valide pendant 1 jour

    return jwt.sign({
        user:{
            id:user.id
        },
        exp:parseInt(expiry.getTime()/1000)
    },process.env.JWT_SECRET);//returning the env secret here might wanna change that so i dont need an .env file
}

//login
function login(req,res,next){
    var credentials = req.body;
    if(!req.body.username || !req.body.password){
        sendJSONresponse(res,401,{
            "error":'all fields are required'
        });
        return;
    }
    User.getUserByUsername(credentials.username, function(credentials){
    }).then(function(resp){
        //console.log('resp outside passport',resp);
        passport.authenticate('local',function(err,response,info){
            var token;
            if(err){
                sendJSONresponse(res,401,err);
                return;
            }
            if(response){
                //console.log('resp inside passport',resp);
                //console.log('response inside passport',response);
                token=generateJwt(resp);
                sendJSONresponse(res,200,{
                    "token":token
                });
            }else{
                sendJSONresponse(response,401,info);
            }
        })(req,res,next);
    },function(err){
        return err;
    }).catch(next);
}

//create new user
function createNewUser(req,res,next) {
    if(req.body){
        var newUser = new User({
            name: req.body.name,
            email:req.body.email,
            username: req.body.username,
            password: req.body.password
        });

        User.createUser(newUser, function(err, user){})
            .then(function(){
                res.send({message:"new user registered successfully"});
            },function(err){
                res.send({error:err});
            }).catch(next);
    }
}

module.exports={
    login:login,
    createNewUser:createNewUser
};