(function () {
    // "use strict";

    var express = require('express');
    var router = express.Router();

    //auth
    var jwt=require('express-jwt'),
    auth=jwt({
        secret:process.env.JWT_SECRET,
        userProperty:'payload'
    });
    var ctrlAuth=require('../controllers/userController');
    var ctrlItem=require('../controllers/itemController');

    //ROOT----------------------------------------------------------------------------------------------------------

    router.get('/', function(req, res){
        res.send('hello world');
    });

    //USERS---------------------------------------------------------------------------------------------------------

    //login
    router.post('/api/users/login',ctrlAuth.login);

    //create new user
    router.post('/api/users/register',ctrlAuth.createNewUser);

    //todo: logout function

    module.exports = router;

    //ITEM------------------------------------------------------------------------------------------------------

    //-home
    //GET OPERATIONS - ITEM

    //get all items
    router.get('/api/item',ctrlItem.getItems);
    //get one item by id
    router.get('/api/item/:_id',ctrlItem.getItemById);

    //CRUD OPERATIONS - ITEM

    router.post('/api/item',auth,ctrlItem.addItem);

    router.put('/api/item/:_id',auth,ctrlItem.updateItem);

    router.delete('/api/item/:_id',auth,ctrlItem.deleteItem);

})();