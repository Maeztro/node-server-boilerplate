(function () {
    //"use strict";

    var mongoose = require('mongoose');

    var webSchema = mongoose.Schema({
        username: {
            type: String,
            index:true
        },
        password: {
            type: String
        },
        email: {
            type: String
        },
        name: {
            type: String
        }
    },{collection:'users'});//its important to specify in which collection were searching otherwise mongo return empty arrays

    var User = module.exports = mongoose.model('Users',webSchema);


    module.exports.createUser = function(newUser){
        return new Promise(function(resolve,reject){
            User.create(newUser).then(function(res){
                resolve(res);
            },function(err){
                reject(err);
            });
        })
    };

    module.exports.getUserByUsername = function(username){
        return new Promise(function(resolve,reject){
            var query = {username: username};
            User.findOne(query).then(function(res){
                //console.log(res);
                resolve(res);
            },function(err){
                //console.log(err);
                reject(err);
            });
        });
    };

    module.exports.getUserById = function(id){
        return new Promise(function(resolve,reject){
            User.findById(id).then(function(res){
                resolve(res);
            },function(err){
                reject(err);
            });
        });
    };

    module.exports.compareUsernames = function(candidateUsernames,userUsernames){
        return(candidateUsernames == userUsernames);
    };

    module.exports.comparePassword = function(candidatePassword,userPassword){
        return(candidatePassword == userPassword);
    };

})();

