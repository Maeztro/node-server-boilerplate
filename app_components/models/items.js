(function () {

    var mongoose = require('mongoose');

    var webSchema = mongoose.Schema({
        title:{
            type:String,
            required:true
        },
        description:{
            type:String,
            required:true
        },
        src:{
            type:String,
            required:true
        },
        link:{
            type:String,
            required:true
        },
        url:{
            type:String,
            required:true
        },
        category:{
            type:String,
            required:true
        },
        belongsto:{
            type:String,
            required:true
        }
    },{collection:'item'});//its important to specify in which collection were searching otherwise mongo return empty arrays

    var Item = module.exports = mongoose.model('Item',webSchema);

    module.exports.getItems = function(){
        return new Promise(function(resolve,reject){
            Item.find()
            .then(function(res){
                //console.log('in model: getItems resolved',res);
                resolve(res);
            },function(err){
                reject(err);
            });
        });
    };

    module.exports.getItemById = function(id){
        return new Promise(function(resolve,reject){
            Item.findById(id)
            .then(function(res){
                //console.log('in model: getItemById resolved',res);
                resolve(res);
            },function(err){
                reject(err);
            });
        });
    };

    module.exports.addItem = function(args){
        return new Promise(function(resolve,reject){
            Item.create(args)
            .then(function(res){
                //console.log('in model: addItem resolved',res);
                resolve(res);
            },function(err){
                reject(err);
            });
        });
    };

    module.exports.updateItem = function(args,options){
        //console.log('in model: updateItem recieved',args);
        return new Promise(function(resolve,reject){
            var query = {_id:args._id};
            var update = {
                title:args.title,
                description:args.description,
                src:args.src,
                link:args.link,
                url:args.url
            };
            Item.findByIdAndUpdate(query,update,options)
                .then(function(res){
                //console.log('in model: findByIdAndUpdate resolved',res);
                resolve(res);
            },function(err){
                    //console.log('in model: findByIdAndUpdate caused an error',err);
                reject(err);
            });
        });
    };

    module.exports.deleteItem = function(args){
        return new Promise(function(resolve,reject){
            var query = {_id:args};
            Item.remove(query)
                .then(function(res){
                    //console.log('in model: deleteItem resolved',res);
                    resolve(res);
                },function(err){
                    reject(err);
                });
        });
    };

})();
